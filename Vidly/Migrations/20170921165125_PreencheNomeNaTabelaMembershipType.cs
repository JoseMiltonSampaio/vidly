﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Vidly.Migrations
{
    public partial class PreencheNomeNaTabelaMembershipType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("UPDATE MembershipType set Name = 'Sem desconto' where DurationInMonths = 0;");
            migrationBuilder.Sql("UPDATE MembershipType set Name = 'Mensal' where DurationInMonths = 1;");
            migrationBuilder.Sql("UPDATE MembershipType set Name = 'Trimestral' where DurationInMonths = 3;");
            migrationBuilder.Sql("UPDATE MembershipType set Name = 'Anual' where DurationInMonths = 12;");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
