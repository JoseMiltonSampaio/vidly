﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Vidly.Migrations
{
    public partial class PopulateGenre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Genre values ('Comedy');");
            migrationBuilder.Sql("INSERT INTO Genre values ('Action');");
            migrationBuilder.Sql("INSERT INTO Genre values ('Family');");
            migrationBuilder.Sql("INSERT INTO Genre values ('Romance');");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
